import logging
import threading
from threading import Thread
import time

class MyTimerThread(Thread):
    time

    def __init__(self, time):
        Thread.__init__(self)
        self.time = time
        return

    def run(self):
        time.sleep(self.time * 60)
        print("Timer complete.")
        return
    
