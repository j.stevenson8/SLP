import re
from User import User
from userInit import userInit
from getInput import getInput
from Calculator import Calculator
from MyTimerThread import MyTimerThread
import threading
import logging
import time
import webbrowser
from userAdd import userAdd
from searchUsers import searchUsers

#default variables
searchEngine = "google.com/"
userFile = "user.txt"

#load database
database = userInit(userFile)

#ask the user for their identity
print("Hello, what is your name?")

#get input from the user
inp = getInput()
#print(in)

#search database for their name
user = searchUsers(database, inp, userFile)

#create a flag to terminate the program
flag = True

while(flag):

    #get a command from the user
    print("What would you like me to do?")
    inp = getInput()

    #parse input into a list of words
    text = inp.split(" ")
    #print(text)

    #search for command
    command = text[0]

    if(command == "exit"):
        print("Goodbye.")
        flag = False
    elif(command == "add"):
        print("Adding " + text[1] + " and " + text[3])
        print(Calculator.add(int(text[1]), int(text[3])))
    elif(command == "subtract"):
        print("Subtracting " + text[1] + " and " + text[3])
        print(Calculator.subtract(int(text[1]), int(text[3])))
    elif(command == "multiply"):
        print("Multiplying " + text[1] + " and " + text[3])
        print(Calculator.multiply(int(text[1]), int(text[3])))
    elif(command == "divide"):
        print("Dividing " + text[1] + " and " + text[3])
        print(Calculator.divide(int(text[1]), int(text[3])))
    elif(command == "set"):
        if(text[1] == "timer"):
            timer = MyTimerThread(int(text[2]))
            timer.start()
            print("Timer set for " + text[2] + " minutes.")
    elif(command == "search"):
        print("Searching for " + text[2] + " on Google.")
        webbrowser.open(searchEngine+text[2], new = 1)
    else:
        print("Sorry, I don't know that command.")



