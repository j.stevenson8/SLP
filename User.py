class User:
    firstName = ""
    lastName = ""
    id = 0
    isAdmin = False

    def __init__(self, first, last, id, isAdmin):
        self.firstName = first
        self.lastName = last
        self.id = id
        self.isAdmin = isAdmin
        return

    def getFirst(self):
        return (self.firstName)

    def getLast(self):
        return (self.lastName)
    
    def greet(self):
        print("Welcome back, " + self.firstName + ".")

    def farewell(self):
        print("Goodbye, " + self.lastName + ".")

    def __str__(self):
        return (self.firstName + ' ' + self.lastName)
