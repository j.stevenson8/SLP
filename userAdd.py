def userAdd(user, fileName):
    f = open(fileName, "r")
    
    users = f.readlines()
    user.id = str(int(users[len(users)-1].split(" ")[0]) + 1)

    f.close()
    f = open(fileName, "a")
    f.write(user.id + " " + user.lastName + ", " + user.firstName + " " + str(user.isAdmin))
    f.close()    
