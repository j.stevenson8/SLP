import re
from User import User

def userInit (file):

    #open the file
    try:
        f = open(file)
    except:
        pass

    #make a list of users
    database = []

    #make a list for reading in the file
    names = []

    #read from file
    try:
        names = f.readlines()
        #print(names)
    except:
        pass

    #initialize id count
    id = 0

    #read the file until there is nothing left
    for name in names:
        
        #parse first and last name from each line
        temp = re.split('\W+', name)
    
        #add new entry to the database
        database.append(User(temp[2], temp[1], temp[0], temp[3]))
        
    for i in database:
        print(i)

    return (database)
